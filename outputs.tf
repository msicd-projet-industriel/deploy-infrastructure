output "external_bastion_ip" {

    description = "adresse ip publique de bastion"
    value = module.os-bastion.os_bastion_ext_ip.address

}


output "external_application_ip" {

    description = "adresse ip publique des noeuds app"
    value = module.os-cluster.os_node_ext_ip.*.address

}