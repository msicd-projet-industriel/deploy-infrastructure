output "os_cidrs_internal" {
    description = "blocs ip du projet os"
    value = var.os_cidrs.internal
}

# Recuperer l"id du reseau de securite 
output "os_network_name" {
    description = "nom de reseau du projet os"
    value = "${openstack_networking_network_v2.os_network.name}"
}


output "os_network_internal" {
    description = "sous-reseaux interne du projet os"
    value = var.os_network.internal
}

