# Blocs ip de l'infra  os
variable "os_cidrs"  {
  type = map
  default = {
        internal = "192.168.1.0/24"
  }
  description = "blocs ip du projet os"
}

# Nom de reseaux a transmettre en output pour les autres modules
variable "os_network" {
  type = map
  default = {
        name = "os_net"
        internal = "os_subnet"
  }
  description = "Details du reseau du projet os"
}

# Info routeur
variable "os_router" {
  type = map
  default = {
    name = "os_router"
    gateway = "external"
  }
}

# dns ip list
variable "os_subnet_dns" {
  description = "Liste des serveurs dns"
  type        = list(string)
  default     = [ "192.44.75.10"]
  
}

