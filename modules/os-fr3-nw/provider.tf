# Definir le provider a utiliser
terraform {
required_version = ">= 0.14.0"
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "~> 1.48.0"
    }
  }
}
# ne pas configurer les credentials de openstack donc '{}'
# nous allons faire un export du fichier de orizon a la place => source openrc-demo
provider openstack {}
