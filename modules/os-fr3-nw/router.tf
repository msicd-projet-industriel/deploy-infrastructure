#
# ------------ ROUTER --------------
###############################################
# Ajouter au routeur comme passerelle le routeur du reseau "external"
# internet_gw => nom de ref dans terraform
data "openstack_networking_network_v2" "os_internet_gw" {
  # nom de ref dans openstack du reseau externe
  name = var.os_router["gateway"]
}

# Creer un routeur 
resource "openstack_networking_router_v2" "os_router" {
  name                = var.os_router["name"]
  admin_state_up      = true
  # route par defaut
  # recupere id de 'internet_gw' par avec ressource data
  external_network_id = "${data.openstack_networking_network_v2.os_internet_gw.id}"

}

# Ajouter une interface  #### iface 1 ###
# connectant un sous-reseau au routeur 
resource "openstack_networking_router_interface_v2" "os_router_iface" {
  router_id = "${openstack_networking_router_v2.os_router.id}"
  subnet_id = "${openstack_networking_subnet_v2.os_subnet_internal.id}"
}

