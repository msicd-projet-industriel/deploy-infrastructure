#------------------------------------------------------
#      Groupes de securite pour acces externes        -
#------------------------------------------------------

# Declaration de groupe de securite pour application
resource "openstack_networking_secgroup_v2" "os_cluster_sg_app" {
  name        = "os_cluster_sg_app"
  description = "Groupe de securite pour application"
}

# Rule   TCP HTTP 80 ALLOWED FROM ALL
resource "openstack_networking_secgroup_rule_v2" "os_app_http_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_app.id}"
}

# Rule   TCP HTTPS  443 ALLOWED FROM ALL
resource "openstack_networking_secgroup_rule_v2" "os_app_https_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_app.id}"
}

# # Rule   TCP HTTPS  8002 ALLOWED FROM ALL (vapormap app)
# resource "openstack_networking_secgroup_rule_v2" "os_app_vapormap_ingress" {
#   direction         = "ingress"
#   ethertype         = "IPv4"
#   protocol          = "tcp"
#   port_range_min    = 8002
#   port_range_max    = 8002
#   remote_ip_prefix  = "0.0.0.0/0"
#   security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_app.id}"
# }

# # Rule   TCP HTTPS  5001 ALLOWED FROM ALL (vapormap API)
# resource "openstack_networking_secgroup_rule_v2" "os_app_vapormapapi_ingress" {
#   direction         = "ingress"
#   ethertype         = "IPv4"
#   protocol          = "tcp"
#   port_range_min    = 5001
#   port_range_max    = 5001
#   remote_ip_prefix  = "0.0.0.0/0"
#   security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_app.id}"
# }




#------------ controlplane --

# Declaration de groupe de securite pour controlplane
resource "openstack_networking_secgroup_v2" "os_cluster_sg_controlplane" {
  name        = "os_cluster_sg_controlplane"
  description = "Groupe de securite pour reseau interne"
}

# # Rule   TCP HTTPS 6443 ALLOWED FROM ALL kube-api
# resource "openstack_networking_secgroup_rule_v2" "os_controlplane_api_ingress" {
#   direction         = "ingress"
#   ethertype         = "IPv4"
#   protocol          = "tcp"
#   port_range_min    = 6443
#   port_range_max    = 6443
#   remote_ip_prefix  = "0.0.0.0/0"
#   security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_controlplane.id}"
# }


# Rule   TCP HTTPS 9000 ALLOWED FROM ALL traefik loadbalancer dashboard
resource "openstack_networking_secgroup_rule_v2" "os_controlplane_traefik_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 9000
  port_range_max    = 9000
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_controlplane.id}"
}

#-----------------------------------------------------
#     Groupes de securite pour le reseau interne     -
#-----------------------------------------------------
# Declaration de groupe de securite pour les noeuds internes
resource "openstack_networking_secgroup_v2" "os_cluster_sg_internal" {
  name        = "os_cluster_sg_internal"
  description = "Groupe de securite pour reseau interne"
}


# Rule   ALL TCP/UDP ALLOWED 
resource "openstack_networking_secgroup_rule_v2" "os_cluster_internal_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = var.os_cidrs_internal
  security_group_id = "${openstack_networking_secgroup_v2.os_cluster_sg_internal.id}"
}





