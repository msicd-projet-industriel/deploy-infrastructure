
#-------------------------
#        MASTER NODES
#-------------------------
#             
resource "openstack_compute_instance_v2" "os_master" {
  count = length(var.os_vm_master)
  name  = var.os_vm_master[count.index]
  image_name = var.os_images["ubuntu"]
#  flavor_name = var.os_flavors["medium"]
  flavor_name = var.os_flavors["large"]
  key_pair        = var.os_public_key

  security_groups = [
    "${openstack_networking_secgroup_v2.os_cluster_sg_internal.id}",
    "${openstack_networking_secgroup_v2.os_cluster_sg_controlplane.id}"
    ]

  network {
    name = var.os_network_name
    fixed_ip_v4 = var.os_master_static_ip[count.index]
  }

  # # Recuperer IP instance
  # provisioner "local-exec" {
  #   command = <<EOT
  #     echo ${self.access_ip_v4} ${self.name}  >> ./hosts.txt
  #     echo ${self.access_ip_v4}   >> ./app_hosts_list.txt
  #   EOT  

  # } 
  tags =  ["master"]

    depends_on = [
    var.os_network_internal,
  ]
  
  
}

#-----------------------
#     WORKER NODES
#-----------------------

resource "openstack_compute_instance_v2" "os_node" {
  count = length(var.os_vm_nodes)
  name  = var.os_vm_nodes[count.index]
  image_name = var.os_images["ubuntu"]
  flavor_name = var.os_flavors["xlarge"]
  key_pair        = var.os_public_key
  security_groups = [
    "${openstack_networking_secgroup_v2.os_cluster_sg_app.id}",
    "${openstack_networking_secgroup_v2.os_cluster_sg_internal.id}"
    ]

  network {
    name = var.os_network_name
  } 
  tags =  ["worker"]

    depends_on = [
    var.os_network_internal,
  ]
  

}




# ==== IP flottantes et IP statiques ====           
## pour application (node01) --  allouer une IP externe puis associer a l'instance
# os_node[0] =  os-node01
# os_node[0].network[0].fixed_ip_v4 => IP statique 1ere interface de node01

#-----------------------------------
#   FIP  MASTERS (static mapping)
#-----------------------------------
#-- Une seule IP pour le 1er noeud 
resource "openstack_networking_floatingip_v2" "os_master_fip" {
  pool = "external"
}
resource "openstack_compute_floatingip_associate_v2" "os_master_ipassoc" {
  floating_ip = "${openstack_networking_floatingip_v2.os_master_fip.address}"
  instance_id = "${openstack_compute_instance_v2.os_master[0].id}"
  fixed_ip    = "${openstack_compute_instance_v2.os_master[0].network[0].fixed_ip_v4}"
}

#--- Utiliser ces deux ressources si une IP pour chaque master --

# resource "openstack_networking_floatingip_v2" "os_master_fip" {
#   count = length(var.os_vm_master)
#   pool = "external"
# }
# resource "openstack_compute_floatingip_associate_v2" "os_master_ipassoc" {
#   count = length(var.os_vm_master)
#   floating_ip = "${openstack_networking_floatingip_v2.os_master_fip[count.index].address}"
#   instance_id = "${openstack_compute_instance_v2.os_master[count.index].id}"
#   fixed_ip    = "${openstack_compute_instance_v2.os_master[count.index].network[0].fixed_ip_v4}"
# }

#-----------------------------------
#     FIP WORKERS (synamic)
#-----------------------------------
resource "openstack_networking_floatingip_v2" "os_node_fip" {
  count = length(var.os_vm_nodes)
  pool = "external"
}

resource "openstack_compute_floatingip_associate_v2" "os_node_ipassoc" {
  count = length(var.os_vm_nodes)
  floating_ip = "${openstack_networking_floatingip_v2.os_node_fip[count.index].address}"
  instance_id = "${openstack_compute_instance_v2.os_node[count.index].id}"
  #fixed_ip    = "${openstack_compute_instance_v2.os_node[count.index].network[0].fixed_ip_v4}"
}
