
# Liste des instances

output "os_master_list" {
    description = "Liste des instances ayant le role app "
    value = "${openstack_compute_instance_v2.os_master}"
}

output "os_node_list" {
    description = "Liste des instances ayant le role node "
    value = "${openstack_compute_instance_v2.os_node}"
}


# IP externe Node app. Attention .*. mis pour lister toutes les adresses externes
output "os_node_ext_ip" {
    description = "IP externe(s) instance(s) application "
    value = "${openstack_networking_floatingip_v2.os_node_fip}"
}
