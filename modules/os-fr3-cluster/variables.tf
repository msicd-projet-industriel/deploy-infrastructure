# IP d'administration distance
variable "os_remote_admin_ip" {}

# chemin de la cle prive ssh
variable "os_public_key" {}
# variable "os_openstack_key_path" {}

# Noms des instances openstack 
variable "os_vm_master" {
  description = "Liste des VM app du projet os"
  type        = list(string)
  default     = [ "ms-icd-vm"]
}

variable "os_vm_nodes" {
  description = "Liste des VM nodes du projet os"
  type        = list(string)
  default     = [ "ms-icd-node"]
}


# Noms des images  
variable "os_images" {
   type = map
   description = "Liste des images pour le projet os"
   default = {
      ubuntu = "imta-docker"
   }
}

# Noms des gabarits pour le projet  
variable "os_flavors" {
   type = map
   description = "Liste des gabrits pour le projet os"
   default = {
      # 1024M mem. 10G ssd.
      small = "s10.small"
      # 2048M mem. 20G ssd.
      medium = "s20.medium"
      # 4G mem. 20G ssd.
      large = "s20.large"
      # 6G mem. 20G ssd.
      xlarge-6 = "s20.xlarge.6g"
      # 8G mem. 20G ssd
      xlarge = "s20.xlarge"
   }
}

# Info adressage reseau de l'infra  os 
# ( autant d'IP statiques que d'instances app => os_vm_master)
variable "os_master_static_ip" {
  description = "IP statiques des noeuds du os"
  type        = list(string)
  default     = [ "172.16.200.11"]
}


# Nom de reseau

variable "os_cidrs_internal"  {}

variable "os_network_name" {}

variable "os_network_internal" {}
