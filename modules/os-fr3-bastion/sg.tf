##
# --- Groupes de securite pour bastion ---
#
resource "openstack_networking_secgroup_v2" "os_admin_sg_bastion" {
  name        = "os_admin_sg_bastion"
  description = "Groupe de securite pour vm bastion"
}
# Rule   SSH 22 ALLOWED pour bastion
resource "openstack_networking_secgroup_rule_v2" "bastion_ssh_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = var.os_remote_admin_ip
  security_group_id = "${openstack_networking_secgroup_v2.os_admin_sg_bastion.id}"
}

##
# --- Groupes de securite pour le reseau interne  ---
#
resource "openstack_networking_secgroup_v2" "os_admin_sg_internal" {
  name        = "os_admin_sg_internal"
  description = "Groupe de securite pour reseau interne"
}
# Rule   ALL TCP/UDP ALLOWED 
resource "openstack_networking_secgroup_rule_v2" "app_internal_ingress" {
  direction         = "ingress"
  ethertype         = "IPv4"
  port_range_min    = 0
  port_range_max    = 0
  remote_ip_prefix  = var.os_cidrs_internal
  security_group_id = "${openstack_networking_secgroup_v2.os_admin_sg_internal.id}"
}

