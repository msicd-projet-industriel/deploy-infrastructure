# IP d'administration distance
variable "os_remote_admin_ip" {}
variable "remote_user" {
   type = string
   default = "ubuntu"
}

# chemin de la cle prive ssh
variable "os_public_key" {}
variable "os_openstack_key_path" {}

# Noms des instances openstack 
variable "os_vm_bastion" {
  description = "Nom instance  bastion"
  default     = "os-bastion"
}

#variable "os_nodes_list" {}
#variable "os_master_list" {}

# Noms des images  
variable "os_images" {
   type = map
   description = "Liste des images pour le projet os"
   default = {
      ubuntu = "imta-docker",
      ubuntu2 = "imta-ubuntu20"
   }
}

# Noms des gabarits pour le projet  
variable "os_flavors" {
   type = map
   description = "Liste des gabrits pour le projet os"
   default = {
      # 1024M mem. 10G ssd.
      small = "s10.small"
      # 2048M mem. 10G ssd.
      medium = "s10.medium"
   }
}

# Info adressage infra  os
# ( autant d'IP statiques que d'instances bastion => os_vm_bastion)
variable "os_vm_bastion_static_ip" {
  description = "IP statique machine bastion"
  default     = "172.16.200.10"
}

# Nom de reseau recupere par outputs
variable "os_cidrs_internal"  {}

variable "os_network_name" {}

variable "os_network_internal" {}

# Listes des instances recuperer par outputs
variable "os_node_list" {}
variable "os_master_list" {}


# Version Ansible utilisee
variable "os_ansible_vervion" {
   type = string
   default = "7.2.0-r0"
}

# Type d deploiement
variable "os_cluster_upgrade" {
   type = bool
   default = "false"
}


