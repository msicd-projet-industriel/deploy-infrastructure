# Listes des instances recuperer par outputs
variable "os_node_list" {}
variable "os_master_list" {}
variable "os_bastion_ext_ip" {}
variable "os_node_ext_ip" {}
variable "remote_user" {
   type = string
   default = "ubuntu"
}


# chemin de la cle prive ssh
# Obligatoirement dans le repertoire local
variable "os_openstack_key_path" {
   type = string
   default = ""
}