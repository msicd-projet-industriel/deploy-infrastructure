>_Date:_ Lundi 21 MARS 2023.
>
> **Mastère Spécialisé - Infrastructure Cloud et  DevOps 2022-23**
>
> GROUPE 3
>

# Projet: Mise en oeuvre d'un environnement de type cluster 

##  Présentation
Ce dépôt permet de déployer une infrastructure  **Openstack**  avec la chaîne de déploiement continue Gitlab de ce sous-projet pour accueillir un cluster de deux groupes d'instances principaux. 

Des modules terraform sont utilisés pour créer ces instances. Il s'agit de:

- [x] `os-network` assurant l'orchestration du réseau;
- [x] `os-cluster` pour le déploiement des instances masters et workers;
- [x] `os-bastion` pour le déploiement de l'instance bastion qui permet de faire le rebond pour configurer les autres instances;
- [x] `os-inventory` pour générer et mettre à jour le fichier d'inventaire.


## Utilisation du dépôt
Le fichier de chaîne de déploiement continue `.gitlab-ci.yml` dans ce dépôt construit une image d'exécution de job terraform à l'aide du Dockerfile officiel et un script maintenus par GitLab Inc.
Les evènements de commit déclenche automatiquement tous les jobs des 03 premiers stages prepare, validate et build. Le fichier d'état tfstate est géré par le backend terraform de GitLab.

### Pré-requis

Pour personnaliser le déploiement dans un autre projet Openstack, il faut renseigner les variables Gitlab de type  fichier suivants:

- `OPENRC_DEMO`: même contenu que le fichier de script `Openstack RC` qui permet l'identification keystone de récupération du jeton d'accès aux services et aussi les paramètres de projet openstack;
- `OPENSTACK_PRIV_KEY`: la clé privée SSH dont la clé publique est importée dans Openstack;
- `OPENSTACK_PUB_KEY`: la clé publique correspondant à la clé privée ci-dessus;
- `PRODUCTION_AUTO_TFVARS`: contient les variables qui permettent de modifier l'environnement du cluster (réseau, liste des instances, clé SSH, ...).

La variable Gitlab optionnelles:

- `TF_CLUSTER_VERSION`: en cas de changement de version de l'image de job.


### Les valeurs des variables terraform dans la variable de type fichier `PRODUCTION_AUTO_TFVARS`

Ci-après les valeurs par défaut des variables importantes:

```bash
#---------------------------------
#       Definition du reseau
#---------------------------------
# Blocs ip de l'infra  os
os_cidrs = {
   internal = "172.16.30.0/24"
}

# Noms reseau et sous-rx de l'infra
os_network = {
   name = "k8s_net"
   internal = "k8s_subnet"
}

# infos routeur de os
os_router = {
   name = "k8s_router"
   gateway = "external"
}
os_subnet_dns = ["192.44.75.10"]

#---------------------------------
#         Administration
#---------------------------------

# Administration distance
os_remote_admin_ip = "0.0.0.0/0"

# nom de la cle prive ssh dans openstack
os_public_key = "demo"


#--------------------------------------
#  Definition des instances du cluster
#--------------------------------------

# instances du Controlplane
os_vm_master = ["control1","control2","control3"]

# inatances workers
os_vm_nodes = ["worker1","worker2"]

# autant d'IP statiques que d'instances master => os_vm_master)
os_master_static_ip = ["172.16.30.201","172.16.30.202","172.16.30.203"]


#---------------------------------
#   Definition instance bastion
#---------------------------------

# bastion
os_vm_bastion = "bastion"

# autant d'IP statiques que d'instances bastion => os_vm_bastion)
os_vm_bastion_static_ip = "172.16.30.200"

# (injecté par la ci) 
# Dans ce module, la cle est obligatoirement dans le repertoire racine
# de projet, pas de / ni ~ 
os_openstack_key_path = "demo-openstack.pem"

```

`os_public_key` désigne le nom de la clé publique importée dans openstack. Il est important de le renseigner dans la variable Gitlab de type fichier `PRODUCTION_AUTO_TFVARS` si différent de `demo` sinon importer la nouvelle clé publique dans  openstack sous ce nom.  Le bloc CIDR et les adresses IP sont liées. Les adresses IP doivent être dans la plage du bloc retenu.

### Déploiement de l'infrastructure système du cluster

- [x] Enregistrer un runner 

Le runner doit pouvoir faire du Docker in Docker (DinD) avec les tags iac, master.

```bash
sudo gitlab-runner register --url https://gitlab.imt-atlantique.fr \
       --name docker-executor-for-vapormap \
       --tag-list iac,master --executor docker
```
- [x] Modifier les variables Gitlab liées au nouvel environnement selon les pré-requis, notamment `OPENRC_DEMO`, `OPENSTACK_PRIV_KEY`, `OPENSTACK_PUB_KEY` et `PRODUCTION_AUTO_TFVARS`.

- [x] Importer la clé publique SSH dans Openstack sous le nom `demo` pour faire simple ou un autre nom si nécessaire;


- [x] Déclencher manuellement un nouveau cycle d'exécution du pipeline GitLab (CI/CD > Pipelines > Bouton Run pipeline)

- [x] Déclencher manuellement l'exécution des jobs du stage `deploy` pour appliquer les modifications selon l'état référencé dans le backend par les stages précédants.


### Test

Pour vérifier que l'infrasctructure est en place, il faut relever les adresses IP de bastion et des instances des noeuds application renvoyées en outputs dans le job `deploy`. 

L'interraction par rebond peut se faire en téléchargeant une copie des fichiers d'artefact `ansible.cfg`, `ssh.cfg` et `inventory`.

Exemple :
```sh
Outputs:

external_application_ip = [
  "10.29.244.11",
  "10.29.244.144",
]
external_bastion_ip = "10.29.245.37"
```


