


# Chemin de la cle prive ssh
# Obligatoirement dans le repertoire local
variable "os_openstack_key_path" {
   type = string
   default = ""
}


# IP d'administration distance
variable "os_remote_admin_ip" {}

# Nom de l'utilisateur pour les connections distantes
variable "remote_user" {
   type = string
   default = "ubuntu"
}

# chemin de la cle privee ssh
variable "os_public_key" {}

# Noms de l'instance bastion 
variable "os_vm_bastion" {
  description = "Nom instance  bastion"
  default     = "os-bastion"
}

# Noms des images  
variable "os_images" {
   type = map
   description = "Liste des images pour le projet os"
   default = {
      ubuntu = "imta-docker",
      ubuntu2 = "imta-ubuntu20"
   }
}

# Noms des gabarits pour le projet  
variable "os_flavors" {
   type = map
   description = "Liste des gabrits pour le projet os"
   default = {
      # 1024M mem. 10G ssd.
      small = "s10.small"
      # 2048M mem. 20G ssd.
      medium = "s20.medium"
      # 4G mem. 20G ssd.
      large = "s20.large"
      # 6G mem. 20G ssd.
      xlarge-6 = "s20.xlarge.6g"
      # 8G mem. 20G ssd
      xlarge = "s20.xlarge"
   }
}


#--Infos adressage 

# Blocs ip de l'infra  os
variable "os_cidrs"  {
  type = map
  default = {
        internal = "192.168.1.0/24"
  }
  description = "blocs ip du projet os"
}

# IP statique de l'instance bastion
variable "os_vm_bastion_static_ip" {
  description = "IP statique machine bastion"
  default     = "172.16.200.10"
}

# IP statiques instances du controlplane
# ( autant d'IP statiques que d'instances app => os_vm_master)
variable "os_master_static_ip" {
  description = "IP statiques des noeuds du os"
  type        = list(string)
  default     = [ "172.16.200.11"]
}

# Noms de reseaux a transmettre en output pour les autres modules
variable "os_network" {
  type = map
  default = {
        name = "os_net"
        internal = "os_subnet"
  }
  description = "Details du reseau du projet os"
}

# Info routeur
variable "os_router" {
  type = map
  default = {
    name = "os_router"
    gateway = "external"
  }
}

# Liste des serveur DNS 
variable "os_subnet_dns" {
  description = "Liste des serveurs dns"
  type        = list(string)
  default     = [ "192.44.75.10"]
  
}


# Noms des instances du groupe masters
variable "os_vm_master" {
  description = "Liste des VM app du projet os"
  type        = list(string)
  default     = [ "ms-icd-vm"]
}

variable "os_vm_nodes" {
  description = "Liste des VM nodes du projet os"
  type        = list(string)
  default     = [ "ms-icd-node"]
}

