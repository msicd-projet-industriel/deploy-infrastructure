
module "os-network" {
   source = "./modules/os-fr3-nw"
   # Blocs ip de l'infra  os
   os_cidrs = "${var.os_cidrs}"
   # Noms reseau et sous-rx de l'infra os
   os_network = "${var.os_network}"
   # infos routeur de os
   os_router = "${var.os_router}"
   os_subnet_dns = "${var.os_subnet_dns}"

}


module "os-cluster" {
   source = "./modules/os-fr3-cluster"

   # Output du module os-network
   os_cidrs_internal = "${module.os-network.os_cidrs_internal}"
   os_network_name = "${module.os-network.os_network_name}"
   os_network_internal = "${module.os-network.os_network_internal}"

   # Administration distance
   os_remote_admin_ip = "${var.os_remote_admin_ip}"
   # nom de la cle prive ssh dans openstack
   os_public_key = "${var.os_public_key}"
   # app
   os_vm_master = "${var.os_vm_master}"
   #os_vm_master = ["control1","control2","control3"]
   # workers
   os_vm_nodes = "${var.os_vm_nodes}"
   #os_vm_nodes = ["worker1"]
   # autant d'IP statiques que d'instances master => os_vm_master)
   os_master_static_ip = "${var.os_master_static_ip}"

   depends_on = [
     module.os-network,
   ]
   
}


module "os-bastion" {
   source = "./modules/os-fr3-bastion"

   # Outputs du module os-network
   os_cidrs_internal = "${module.os-network.os_cidrs_internal}"
   os_network_name = "${module.os-network.os_network_name}"
   os_network_internal = "${module.os-network.os_network_internal}"

   # Ouputs module cluster
   os_node_list = "${module.os-cluster.os_node_list}"
   os_master_list = "${module.os-cluster.os_master_list}"
   
   # Administration distance
   os_remote_admin_ip =  "${var.os_remote_admin_ip}"
   os_openstack_key_path = "${var.os_openstack_key_path}"
   # nom de la cle prive ssh dans openstack
   os_public_key = "${var.os_public_key}"
   # bastion
   os_vm_bastion = "${var.os_vm_bastion}"
   # autant d'IP statiques que d'instances bastion => os_vm_bastion)
   os_vm_bastion_static_ip = "${var.os_vm_bastion_static_ip}"

   # Les instances de cluster doivent etre cree avant bastion
   depends_on = [
     module.os-cluster,
   ]


   
}


module "os-inventory" {
   source = "./modules/os-fr3-inventory"
   # Ouputs from  cluster module
   os_node_list = "${module.os-cluster.os_node_list}"
   os_master_list = "${module.os-cluster.os_master_list}"
   os_node_ext_ip = module.os-cluster.os_node_ext_ip

   # Outputs form bastion module
   os_bastion_ext_ip = "${module.os-bastion.os_bastion_ext_ip}"
   
   # (injecte par la ci) 
   # Dans ce module, la cle est obligatoirement dans le repertoire 
   # du projet, pas de / ni ~ 
   os_openstack_key_path = "${var.os_openstack_key_path}"

   # Le reseau doit etre completement a jour avant l'inventaire
   depends_on = [
     module.os-network,
   ]
}
